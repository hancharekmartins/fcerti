const express = require('express')
const app = express()
const extenso = require('extenso')

app.get('/', (req, res) =>
    res.send('Bem-vindo')
)

app.get('/:numero', function(req, res) {
    if(parseFloat(req.params.numero) >= -99999 && parseFloat(req.params.numero) <= 99999) {
            let numeroExtenso = extenso(req.params.numero, { negative: 'informal' })
            res.send({"extenso": numeroExtenso})
        } else {
            res.send("Desculpe, mas o número deve estar entre -99999 a 99999")
        }
})

/*
app.get('/', function(req, res) {
    res.send('Envie sua requisição http para a rota "/:numero" para traduzir por extenso')
})
*/

app.listen(3000, function() {
    console.log(`Listenin at http://localhost:3000`)
})